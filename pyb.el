;;; pyb.el -- Help with running python builder from emacs
;;; Code:

(require 'ido)
(require 'projectile)

(defun get-tasks ()
  (let* ((default-directory (projectile-project-root))
	 (virtual-env (split-string (car process-environment) "="))
	 (pyb-exec (concat (if (equalp (first virtual-env) "VIRTUAL_ENV") (car (last virtual-env)) "") "/bin/pyb"))
	 (tasks (shell-command-to-string (concat pyb-exec " -Qt"))))
    (mapcar (lambda (task) (cons (first (split-string task ":")) pyb-exec)) (split-string tasks "\n"))))

(defun pyb-launch-process (name buffer-name process &rest args) 
  (apply #'start-process name buffer-name process args)
  (switch-to-buffer buffer-name)
  (with-current-buffer buffer-name
    (local-set-key (kbd "C-c C-c") (lambda () (interactive) (kill-process))))
  )

(defun py-build ()
  (interactive)
  (let* ((tasks (get-tasks))
	 (default-directory (projectile-project-root))
	 (task (ido-completing-read "PYB TASK" (mapcar #'first tasks)))
	 (selected-task (assoc task tasks))) 
    (pyb-launch-process "pyb" "*pyb*" (rest selected-task) (first selected-task))))


(provide 'pyb)
